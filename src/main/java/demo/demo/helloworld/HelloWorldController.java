package demo.demo.helloworld;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloWorldController {

    @GetMapping("/helloworld")
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok("Hello World");
    }

    @GetMapping("/fout")
    public ResponseEntity<String> error() {
        return ResponseEntity.badRequest().body("Request not valid");
    }

    @GetMapping("/parameters")
    public ResponseEntity<?> parametersDemo(final @RequestParam("number") String number) {
        try {
            return ResponseEntity.ok(2 * Double.parseDouble(number));
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest().body("Not a number");
        }
    }

    @GetMapping("/parameters2/{number}")
    public ResponseEntity<?> parameters2Demo(final @PathVariable("number") String number) {
        try {
            return ResponseEntity.ok(2 * Double.parseDouble(number));
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest().body("Not a number");
        }
    }

}
